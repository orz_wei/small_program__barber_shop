Page({

  /**
   * 页面的初始数据
   */
data: {
  "userHeader":"../icon/mineIcon/9.jpg",
  "name":"枫叶工作室"
},

/**
 * 生命周期函数--监听页面加载
 */
onLoad: function(options) {

  var _this = this;
  wx.getUserInfo({
    success: function (res) {
      console.log(res);
      var avatarUrl = res.userInfo.avatarUrl;
      var nickName = res.userInfo.nickName;
      var gender = res.userInfo.gender; //性别 0：未知、1：男、2：女 
      var province = res.userInfo.province;
      var city = res.userInfo.city;
      _this.setData({
        "userHeader": avatarUrl,
        "name": nickName,
        "province": province,
        "city": city
      });
    }
  })
  
},

getUserInfo (e) {
    console.log(e.detail.userInfo)
  this.setData({
    userNickName: e.detail.userInfo.nickName,
    userHeader: e.detail.userInfo.avatarUrl
  })
},

getPhoneNumber: function (e) {
    console.log(e.detail.errMsg)
    console.log(e.detail.iv)
    console.log(e.detail.encryptedData)
    if (e.detail.errMsg == 'getPhoneNumber:fail user deny') {
      wx.showModal({
        title: '提示',
        showCancel: false,
        content: '未授权',
        success: function (res) { }
      })
    } else {
      wx.showModal({
        title: '提示',
        showCancel: false,
        content: '同意授权',
        success: function (res) { }
      })
    }
  },

/**
 * 生命周期函数--监听页面初次渲染完成
 */
onReady: function() {

},

/**
 * 生命周期函数--监听页面显示
 */
onShow: function() {

},

/**
 * 生命周期函数--监听页面隐藏
 */
onHide: function() {

},

/**
 * 生命周期函数--监听页面卸载
 */
onUnload: function() {

},

/**
 * 页面相关事件处理函数--监听用户下拉动作
 */
onPullDownRefresh: function() {

},

/**
 * 页面上拉触底事件的处理函数
 */
onReachBottom: function() {

},

/**
 * 用户点击右上角分享
 */
onShareAppMessage: function() {

}
})