var Config = {
     /*
     * service接口配置
     */
     timestampConfig : {
        timestamp_uri: "https://api.mysubmail.com/service/timestamp.json"
     },
    
    /*
     * 短信配置
     */
    messageConfig : {
        xsend_uri : 'https://api.mysubmail.com/message/xsend.json',
        send_uri  : 'https://api.mysubmail.com/message/send.json',
        
      appid: '30410',
      appkey: '0a282f9f9f0abd73b1145ad2d171880f',
        signtype : 'normal'   /*可选参数normal,md5,sha1*/
    },
   

};

module.exports = Config;
